"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import CASCADE
from saudeemjogo.strings import *


class TiposModulo(models.Model):
	"""
		Tipos de módulos
	"""
	nome = models.CharField(max_length=80)

	class Meta:
		verbose_name = 'Tipos de Módulo'
		verbose_name_plural = 'Tipos de Módulos'

	def __str__(self):
		return self.nome

	def save(self, *args, **kwargs):
		self.nome = self.nome.title()
		super().save(*args, **kwargs)

	def to_dict(self):
		return \
			{
				"id": self.id,
				"tipo": self.nome
			}


class Medico(models.Model):
	perfil = models.IntegerField(null=True, blank=False)
	salario = models.IntegerField(null=True, blank=False)
	expertise = models.CharField(max_length=1)
	atendimento = models.CharField(max_length=1)
	pontualidade = models.CharField(max_length=1)

	def save(self, *args, **kwargs):
		if not self.expertise.isalpha():
			return \
				{
					err: "Expertise deve ser uma letra entre A e Z"
				}
		if not self.atendimento.isalpha():
			return \
				{
					err: "Atendimento deve ser uma letra entre A e Z"
				}
		if not self.pontualidade.isalpha():
			return \
				{
					err: "Pontualidade deve ser uma letra entre A e Z"
				}
		self.expertise = self.expertise.upper()
		self.pontualidade = self.pontualidade.upper()
		self.atendimento = self.atendimento.upper()
		super().save(*args, **kwargs)

	def __str__(self):
		return "Médico perfil: " + str(self.perfil)

	def to_dict(self):
		return \
			{
				"id": self.id,
				"salario": self.salario,
				"expertise": self.expertise,
				"atendimento": self.atendimento,
				"pontualidade": self.pontualidade
			}


class Modulo(models.Model):
	"""
		Módulos comprados pelos hospitais
	"""
	nome = models.ForeignKey(TiposModulo, on_delete=CASCADE)
	custo_aquisicao = models.IntegerField(null=True, blank=False)
	custo_por_mes = models.IntegerField(null=True, blank=False)
	tecnologia = models.CharField(max_length=1)
	conforto = models.CharField(max_length=1)
	capacidade = models.IntegerField(null=True, blank=False)
	preco_do_tratamento = models.IntegerField(null=True, blank=False)

	def save(self, *args, **kwargs):
		if not self.tecnologia.isalpha():
			return \
				{
					err: "Tecnologia deve ser uma letra entre A e Z"
				}
		if not self.conforto.isalpha():
			return \
				{
					"Conforto deve ser uma letra entre A e Z"
				}
		self.conforto = self.conforto.upper()
		self.tecnologia = self.conforto.upper()
		super().save(*args, **kwargs)

	class Meta:
		verbose_name = 'Módulo'
		verbose_name_plural = 'Módulos'

	def __str__(self):
		return str(self.nome) + " " + str(self.id)

	def to_dict(self):
		return \
			{
				"id": self.id,
				"nome": str(self.nome),
				"custo_aquisicao": self.custo_aquisicao,
				"custo_por_mes": self.custo_por_mes,
				"tecnologia": self.tecnologia,
				"conforto": self.conforto,
				"capacidade": self.capacidade,
				"preco_do_tratamento": self.preco_do_tratamento,
			}


class MedicoModulo(models.Model):
	medico = models.ForeignKey(Medico, on_delete=CASCADE)
	modulo = models.ForeignKey(Modulo, on_delete=CASCADE)

	class Meta:
		verbose_name = 'Médico no módulo'
		verbose_name_plural = 'Médicos nos módulos'

	def __str__(self):
		return str(self.medico) + " em " + str(self.modulo)

	def to_dict(self):
		return \
			{
				"medico": self.medico.id,
				"modulo": self.modulo.id
			}


class Hospital(models.Model):
	"""
		Hospital organizado pelos jogadores
		Os jogadores alterarão diretamente os hospitais
	"""
	nome = models.CharField(max_length=80)
	caixa = models.IntegerField(null=True, blank=False, default=25000)
	emprestimos = models.IntegerField(null=True, blank=True)
	modulos = models.ManyToManyField(Modulo, blank=True)
	medicos = models.ManyToManyField(Medico, blank=True)
	medicos_modulos = models.ManyToManyField(MedicoModulo, blank=True)

	def save(self, *args, **kwargs):
		if self.caixa < 0:
			return \
				{
					err: "Valor de caixa menor que 0"
				}
		super().save(*args, **kwargs)

	class Meta:
		verbose_name = 'Hospital'
		verbose_name_plural = 'Hospitais'

	def __str__(self):
		return self.nome

	def atribuir_medico_ao_modulo(self, medico, modulo):
		try:
			me = self.medicos.get(id=medico)
		except ObjectDoesNotExist:
			return \
				{
					err: "Médico com id " + str(modulo) + " não existe"
				}
		try:
			mo = self.modulos.get(id=modulo)
		except ObjectDoesNotExist:
			return \
				{
					err: "Módulo com id " + str(modulo) + " não existe"
				}
		mm = self.medicos_modulos.filter(medico=me)
		if mm is not None:
			mm.delete()
		mm = MedicoModulo(medico=me, modulo=mo)
		mm.save()
		self.medicos_modulos.add(mm)
		return \
			{
				"hospital": self.id,
				success: "Médico "+str(medico)+" atribuido ao módulo "+str(modulo)
			}

	def comprar_modulo(self, modulo):
		try:
			m = Modulo.objects.get(id=modulo)
			if m in self.modulos.all():
				return \
					{
						success: "Módulo " + str(
							m.nome) + " já foi comprado anteriormente, e não pode ser comprado novamente por" + self.nome
					}
			valor = m.custo_aquisicao
			if self.realizar_pagamento(valor):
				self.modulos.add(m)
				return \
					{
						success: "Módulo " + str(m.nome) + " comprado por " + self.nome
					}
			else:
				return \
					{
						err: "Caixa insuficiente"
					}
		except ObjectDoesNotExist:
			return \
				{
					err: "Módulo com o id" + str(modulo) + " não existe"
				}
		except Exception as e:
			return \
				{
					err: str(e)
				}

	def vender_modulo(self, modulo):
		# 40% de reembolso, poderíamos fazer um reembolso variável pra dar um grau
		reembolso = 40
		try:
			m = Modulo.objects.get(id=modulo)
			if m in self.modulos.all():
				self.modulos.remove(m)
			else:
				return \
					{
						success: "Módulo " + str(m.nome) + " não existe neste hospital"
					}
			valor = m.custo_aquisicao * (reembolso/100)
			self.adicionar_dinheiro_ao_caixa(valor)
			return \
				{
					success: "Módulo " + str(m.nome) + " removido e reembolsado 40%"
				}
		except ObjectDoesNotExist:
			return \
				{
					err: "Módulo com o id " + str(modulo) + " não existe"
				}
		except Exception as e:
			return \
				{
					err: str(e)
				}

	def realizar_pagamento(self, valor):
		if self.caixa - valor >= 0:
			self.caixa = self.caixa - valor
			self.save()
			return True
		return False

	def adicionar_dinheiro_ao_caixa(self, valor):
		self.caixa = self.caixa + valor
		self.save()

	def to_dict(self):
		modulos = {}
		medicos = {}
		medicos_modulos = {}
		i = 0
		for m in self.medicos.all():
			medicos[i] = m.to_dict()
			i += 1
		i = 0
		for m in self.modulos.all():
			modulos[i] = m.to_dict()
			i += 1
		i = 0
		for m in self.medicos_modulos.all():
			medicos_modulos[i] = m.to_dict()
			i += 1
		return \
			{
				"id": self.id,
				"nome": self.nome,
				"emprestimos": self.emprestimos,
				"modulos": modulos,
				"medicos": medicos,
				"medicos_modulos": medicos_modulos,
			}
