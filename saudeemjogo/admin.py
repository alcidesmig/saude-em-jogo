"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

from django.contrib import admin
from saudeemjogo.models import *

admin.site.register(TiposModulo)
admin.site.register(Modulo)
admin.site.register(Hospital)
admin.site.register(Medico)
admin.site.register(MedicoModulo)
