"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

from django.contrib import admin
from django.urls import path
from saudeemjogo import views

# Eu realmente não sei se é necessário, já que o nginx vai lidar com
prefix = 'api/'

urlpatterns = [
    path(prefix+'admin/', admin.site.urls),
    path(prefix+'all_medicos/', views.get_all_medicos),
    path(prefix+'all_modulos/', views.get_all_modulos),
    path(prefix+'all_hospitais/', views.get_all_hospitais),
    path(prefix+'reembolsar_modulo/<int:hospital>/<int:modulo>', views.reembolsar_modulo),
    path(prefix+'comprar_modulo/<int:hospital>/<int:modulo>', views.comprar_modulo),
    path(prefix+'atribuir_medico_ao_modulo/<int:hospital>/<int:medico>/<int:modulo>', views.atribuir_medico_ao_modulo)
]
