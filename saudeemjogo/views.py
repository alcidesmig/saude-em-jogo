"""

MIT License

Copyright (c) 2018 PET-BCC UFSCar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

"""

from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from saudeemjogo.models import Medico, Modulo, Hospital, TiposModulo
from saudeemjogo.strings import *


def get_all_medicos(request):
	all_medicos = {}
	medicos = Medico.objects.all()
	i = 0
	for m in medicos:
		all_medicos[i] = m.to_dict()
		i += 1
	if not all_medicos:
		return JsonResponse({err: null_med})
	return JsonResponse(all_medicos)


def get_all_modulos(request):
	all_modulos = {}
	modulos = Modulo.objects.all()
	i = 0
	for m in modulos:
		all_modulos[i] = m.to_dict()
		i += 1
	if not all_modulos:
		return JsonResponse({err: null_mod})
	return JsonResponse(all_modulos)


def get_all_hospitais(request):
	all_hospitais = {}
	hospitais = Hospital.objects.all()
	i = 0
	for h in hospitais:
		all_hospitais[i] = h.to_dict()
		i += 1
	if not all_hospitais:
		return JsonResponse({err: null_hosp})
	return JsonResponse(all_hospitais)


def get_all_tipos_de_modulos(request):
	all_tipos = {}
	tipos = TiposModulo.objects.all()
	i = 0
	for t in tipos:
		all_tipos[i] = t.to_dict()
		i += 1
	if not all_tipos:
		return JsonResponse({err: null_tip_mod})
	return JsonResponse(all_tipos)


def comprar_modulo(request, hospital, modulo):
	try:
		hospital = Hospital.objects.get(id=hospital)
		response = hospital.comprar_modulo(modulo)
	except ObjectDoesNotExist:
		response = {err: not_exist_hosp}
	return JsonResponse(response)


def atribuir_medico_ao_modulo(request, hospital, medico, modulo):
	try:
		hospital = Hospital.objects.get(id=hospital)
		response = hospital.atribuir_medico_ao_modulo(medico, modulo)
	except ObjectDoesNotExist:
		response = {err: not_exist_hosp}
	return JsonResponse(response)


def reembolsar_modulo(request, hospital, modulo):
	try:
		hospital = Hospital.objects.get(id=hospital)
		response = hospital.vender_modulo(modulo)
	except ObjectDoesNotExist:
		response = {err: not_exist_hosp}
	return JsonResponse(response)
